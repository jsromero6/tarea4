import pygame
background_color = (250, 250, 250)
graphic_color = (135, 150, 120)
plano_color = (0, 0, 0)
pygame.init()
Dimensiones = (615, 600)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Plano cartesiano")

#CREO EL TEXTO DEL PLANO CARTESIANO
Fuente = pygame.font.Font(None, 20)
X= Fuente.render("X", True, plano_color)
Y = Fuente.render("Y", True, plano_color)
Cuadrado =Fuente.render("Area: 6", True, plano_color)
Uno = Fuente.render("1", True, plano_color)
Dos = Fuente.render("2", True, plano_color)
Uno1 = Fuente.render("1", True, plano_color)
Dos2 = Fuente.render("2", True, plano_color)
Tres = Fuente.render("3", True, plano_color)

Terminar = False

while not Terminar:
     for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True

     Pantalla.fill(background_color)
     pygame.draw.line(Pantalla, plano_color, [595,300],[5,300],3)
     pygame.draw.line(Pantalla, plano_color, [300, 5], [300, 595], 3)
     pygame.draw.rect(Pantalla, graphic_color, (300, 300, 250, 120), 0)
     Pantalla.blit(X, [605, 280])
     Pantalla.blit(Y, [310, 585])
     Pantalla.blit(Uno, [290, 310])
     Pantalla.blit(Dos, [290, 420])
     Pantalla.blit(Uno1, [310, 280])
     Pantalla.blit(Dos2, [420, 280])
     Pantalla.blit(Tres, [530, 280])
     Pantalla.blit(Cuadrado, [400, 350])



     pygame.display.flip()
pygame.quit()